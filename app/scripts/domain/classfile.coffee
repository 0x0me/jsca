'use strict'

window.JSCA or= {}

window.JSCA.Classfile = class Classfile

  constructor: () ->
    @_mapped = undefined

  setMagic: (@_magic)->

  getMagic: () ->
    @_magic

  setMinor: (@_minor)->

  getMinor: () ->
    @_minor


  setMajor: (@_major)->

  getMajor: () ->
    @_major

  setConstantPoolCount: (@_constantPoolCount) ->

  getConstantPoolCount: () ->
    @_constantPoolCount

  setConstantPool: (@_constantPool) ->

  getConstantPool: () ->
    @_constantPool

  setAccessFlags: (@_accessFlags) ->

  getAccessFlags: () ->
    @_accessFlags

  setThisClass: (@_thisClass) ->

  getThisClass: () ->
    @_thisClass

  setSuperClass: (@_superClass) ->

  getSuperClass: () ->
    @_superClass


  setFieldsCount: (@_fieldsCount) ->

  getFieldsCount: () ->
    @_fieldsCount

  setFields: (@_fields) ->

  getFields: () ->
    @_fields

  setMethodsCount: (@_methodsCount) ->

  getMethodsCount: () ->
    @_methodsCount

  setMethods: (@_methods) ->

  getMethods: () ->
    @_methods

  setAttributesCount: (@_attributesCount) ->

  getAttributesCount: () ->
    @_attributesCount

  setAttributes: (@_attributes) ->

  getAttributes: () ->
    @_attributes

  lookupNameIndex: (nameIndex) ->
    cp = @_constantPool
    val = cp[nameIndex-1]
    val.info.bytes

  lookupClassIndex: (classIndex) ->
    cp = @_constantPool
    val= cp[classIndex-1].info.nameIndex
    @lookupNameIndex val

  getCodeId: () ->
    if @_mapped is undefined
     @_initMappingTable()

    @_mapped['Code']


  _initMappingTable: () ->
    cp = @getConstantPool()

    @_mapped = cp.reduce (arr, elem, idx) ->
      if elem.tag == 1
        p = elem.info.bytes
        arr[p] = idx
      arr
    , {}

  constantToString:(c) =>
    "{tag=#{c.tag}, info={length=#{c.info.length}, bytes=#{c.info.bytes}}}"
