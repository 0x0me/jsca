'use strict'

window.JSCA or= {}


window.JSCA.ClassfileParser = class ClassfileParser
  @codeId = undefined

  constructor: () ->

  classfileTypeset =
    classfile:
      magic: ['array', 'uint8', 4]
      minorVersion: 'uint16'
      majorVersion: 'uint16'
      constantPoolCount: 'uint16'
      constantPool: ['array', 'cpInfo', (context) ->
        # JVM Spec 7: The value of the constant_pool_count item is equal to the number of entries in the constant_pool table plus one.
        context.constantPoolCount-1
      ]
      accessFlags: 'uint16'
      thisClass: 'uint16'
      superClass: 'uint16'
      interfacesCount: 'uint16'
      interfaces: ['array', 'uint16', 'interfacesCount']
      fieldsCount: 'uint16'
      fields: ['array', 'fieldInfo', 'fieldsCount']
      methodsCount: 'uint16'
      methods: ['array', 'methodsInfo', 'methodsCount']
      attributesCount: 'uint16'
      attributes: ['array', 'attributeInfo', 'attributesCount']

    cpInfo:
      tag: 'uint8'
      info: ['__constantType', 'tag']

    __constantType: jBinary.Template
      read: (context) ->
        tag = context.tag
        switch tag
          when 1 then @baseType = 'constantUtf8Info'
          when 3 then @baseType = 'constantIntegerInfo'
          when 4 then @baseType = 'constantFloatInfo'
          when 5 then @baseType = 'constantLongInfo'
          when 6 then @baseType = 'constantDoubleInfo'
          when 7 then @baseType = 'constantClassInfo'
          when 8 then @baseType = 'constantStringInfo'
          when 9 then @baseType = 'constantFieldrefInfo'
          when 10 then @baseType = 'constantMethodrefInfo'
          when 11 then @baseType = 'constantInterfaceMethodrefInfo'
          when 12 then @baseType = 'constantNameAndTypeInfo'
          when 15 then @baseType = 'constantMethodHandleInfo'
          when 16 then @baseType = 'constantMethodTypeInfo'
          when 18 then @baseType = 'constantInvokeDynamicInfo'
          else
            throw new TypeError "Unkown tag [#{tag}]"
        @baseRead()

  # TAG ID 1
    constantUtf8Info: jBinary.Template
      baseType:
        length: 'uint16'
        bytes: ['array', 'uint8', 'length']
      read: () ->
        res = @baseRead()
        arr = res.bytes
        `
        for (var i = 0, l = arr.length, s = '', c; c = arr[i++];) {
          s += String.fromCharCode(
            c > 0xdf && c < 0xf0 && i < l - 1
              ? (c & 0xf) << 12 | (arr[i++] & 0x3f) << 6 | arr[i++] & 0x3f
              : c > 0x7f && i < l
              ? (c & 0x1f) << 6 | arr[i++] & 0x3f
              : c
          );
        }
        `
        res.bytes = s;
        res

  # TAG ID 3
    constantIntegerInfo:
      nameIndex: 'uint32'

  # TAG ID 4
    constantFloatInfo:
      nameIndex: 'uint32'

  # TAG ID 5
    constantLongInfo:
      highBytes: 'uint32'
      lowBytes: 'uint32'

  # TAG ID 6
    constantDoubleInfo:
      highBytes: 'uint32'
      lowBytes: 'uint32'

  # TAG ID 7
    constantClassInfo:
      nameIndex: 'uint16'

  # TAG ID 8
    constantStringInfo:
      stringIndex: 'uint16'

  # TAG ID 9
    constantFieldrefInfo:
      classIndex: 'uint16'
      nameAndTypeIndex: 'uint16'

  # TAG ID 10
    constantMethodrefInfo:
      classIndex: 'uint16'
      nameAndTypeIndex: 'uint16'

  # TAG ID 11
    constantInterfaceMethodrefInfo:
      classIndex: 'uint16'
      nameAndTypeIndex: 'uint16'

  # TAG ID 12
    constantNameAndTypeInfo:
      nameIndex: 'uint16'
      descriptorIndex: 'uint16'

  # TAG ID 15
    constantMethodHandleInfo:
      referenceKind: 'uint8'
      referenceIndex: 'uint16'

  # TAG ID 16
    constantMethodTypeInfo:
      descriptorIndex: 'uint16'

  # TAG ID 17
    constantInvokeDynamicInfo:
      bootstrapMethodAttrIndex: 'uint16'
      nameAndTypeIndex: 'uint16'

    fieldInfo:
      accessFlags: 'uint16'
      nameIndex: 'uint16'
      descriptorIndex: 'uint16'
      attributesCount: 'uint16'
      attributes: ['array', 'attributeInfo', 'attributesCount']

    methodsInfo:
      accessFlags: 'uint16'
      nameIndex: 'uint16'
      descriptorIndex: 'uint16'
      attributesCount: 'uint16'
      attributes: ['array', 'attributeInfo', 'attributesCount']

    attributeInfo:
      attributeNameIndex: 'uint16'
      attributeLength: 'uint32'
      #info: ['array', '__attributeType', (context) ->
      #  context.attributeLength
      #]
      info: ['__attributeType', 'attributeLength']

    __attributeType: jBinary.Template
      read: (context) ->

        type = context.attributeNameIndex-1
        ctx = @binary.getContext 'constantPool'
        constant = ctx.constantPool[type]
        if constant is undefined or constant is null
          console.log "Constant [#{constant}]"

        kind = constant.info.bytes
        #console.log "Attribute type [#{type}] / #{kind}"
        switch kind
          when 'ConstantValue' then @baseType = 'constantValueAttribute'
          when 'Code' then @baseType = 'codeAttribute'
          when 'InnerClasses' then @baseType = 'innerClassAttribute'
          when 'SourceFile' then @baseType = 'sourceFileAttribute'
          when 'LineNumberTable' then @baseType = 'lineNumberTableAttribute'
          when 'LocalVariableTable' then @baseType = 'localVariableTable'
          when 'Signature' then @baseType = 'signatureAttribute'
          else
            count = context.attributeLength
            console.log "Reading [#{count}]bytes of unspecified"
            @baseType = ['array','uint8', count ]
        @baseRead()

    constantValueAttribute:
      constantValueIndex: 'uint16'

    codeAttribute:
      maxStack: 'uint16'
      maxLocals: 'uint16'
      codeLength: 'uint32'
      code: ['array', 'uint8', 'codeLength']
      exceptionTableLength: 'uint16'
      exceptionTable: ['array', '__exceptionTable', 'exceptionTableLength']
      attributesCount: 'uint16'
      attributea: ['array', 'attributeInfo', 'attributesCount']

    __exceptionTable:
      startPc: 'uint16'
      endPc: 'uint16'
      handlerPc: 'uint16'
      catchType: 'uint16'

    innerClassAttribute:
      numberOfClasses: 'uint16'
      classes: ['array', '__classes', 'numberOfClasses']

    __classes:
      innerClassInfoIndex: 'uint16'
      outerClassInfoIndex: 'uint16'
      innerNameIndex: 'uint16'
      innerClassAccessFlags: 'uint16'

    sourceFileAttribute:
      sourceFileIndex: 'uint16'

    lineNumberTableAttribute:
      lineNumberTableLength: 'uint16'
      lineNumberTable: ['array','__lineNumberTable', 'lineNumberTableLength']

    __lineNumberTable:
      startPc: 'uint16'
      lineNumber: 'uint16'

    localVariableTable:
      localVariableTableLength: 'uint16'
      localVariableTable: ['array', '__localVariableTable', 'localVariableTableLength']

    __localVariableTable:
      startPc: 'uint16'
      length: 'uint16'
      nameIndex: 'uint16'
      descriptorIndex: 'uint16'
      index: 'uint16'

    signatureAttribute:
      signatureIndex: 'uint16'

  parse: (file,callback) ->
    jBinary.load file, classfileTypeset, (err, binary) ->
    #jBinary.load 'SameNameDifferentTypeAnalyzer.class', classfileTypeset, (err, binary) ->
      if err is null then console.log "Possible error [#{err}]"
      classfile = binary.read 'classfile'
      console.log "classfile [#{classfile}]"
      magic = classfile.magic
      magicStr = magic.map (val) ->
        val.toString(16).toUpperCase()

      console.log "magic [#{magicStr}]"
      console.log "minor version [#{classfile.minorVersion}]"
      console.log "major version [#{classfile.majorVersion}]"
      console.log "constantPoolCount [#{classfile.constantPoolCount}]"

      cl = new JSCA.Classfile()
      cl.setMagic magicStr
      cl.setMinor classfile.minorVersion
      cl.setMajor classfile.majorVersion
      cl.setConstantPoolCount classfile.constantPoolCount
      cl.setConstantPool classfile.constantPool
      cl.setAccessFlags classfile.accessFlags
      cl.setThisClass classfile.thisClass
      cl.setSuperClass classfile.superClass
      cl.setFieldsCount classfile.fieldsCount
      cl.setFields classfile.fields
      cl.setMethodsCount classfile.methodsCount
      cl.setMethods classfile.methods
      cl.setAttributesCount classfile.attributesCount
      cl.setAttributes classfile.attributes

      callback cl





