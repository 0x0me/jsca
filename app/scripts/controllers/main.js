'use strict';

angular.module('jscaApp')
  .controller('MainCtrl', function ($scope, $log, $location, $anchorScroll, $routeParams) {
    $scope.status = (typeof window.FileReader === 'undefined') ? 'FileReader API not supported' : 'Ready';
    $scope.classfile = new JSCA.Classfile();

    $scope.load = function (file) {
      $log.log('Loading classfile...');
      var parser = new JSCA.ClassfileParser($scope.classfile);
      parser.parse(file,function(classfile) {
        $scope.$apply(function() {
          $scope.classfile = classfile;

          console.log('loaded');
          var cid = $scope.classfile.getCodeId();
          console.log('Code @['+cid+']');
        });
      });

    };

    $scope.draw = function () {
      var width =  400;
      var height = 400;
      var g = new Graph();

      //g.edgeFactory.template.style.directed = true;
      var sidx = $scope.classfile.getSuperClass();
      var superClass = $scope.classfile.lookupClassIndex(sidx);
      g.addNode('superclass', {label: superClass });
      var tidx = $scope.classfile.getThisClass();
      var thisClass = $scope.classfile.lookupClassIndex(tidx);
      g.addNode('thisclass', {label: thisClass });
      g.addEdge('thisclass', 'superclass', {directed: true, label: '<<extends>>'});
      /* layout the graph using the Spring layout implementation */
      //var layouter = new Graph.Layout.Spring(g);
      var layouter = new Graph.Layout.OrderedTree(g, ['superclass','thisclass']);
      layouter.layout();

      /* draw the graph using the RaphaelJS draw implementation */
      var renderer = new Graph.Renderer.Raphael('canvas', g, width, height);
      renderer.draw();
    };

    function retrieve(url) {
      var request = new XMLHttpRequest();
      request.onerror = function (evt) {
        $log.log('ERROR: [' + evt + ']');
        $log.log('EVT [' + typeof(evt.target) + ']');
        $log.log('STATUS: [' + evt.target.status + '], text=[' + evt.target.statusText + ']');
      };
      request.open("GET", url, false);
      request.overrideMimeType("text/plain; charset=x-user-defined");
      request.send();
      if ((200 !== request.status) && (0 !== request.status)) {
        throw new Error(request.status + " Retrieving " + url);
      }
      return request;
    }


  });
