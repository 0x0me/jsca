'use strict';

angular.module('jscaApp')
  .directive('dropload', () ->
    template: '<div class="dropload" ></div>'
    restrict: 'E'
    replace: true
    link: (scope, element, attrs) ->
      element.bind "dragover", () ->
        @className="dropload hover"
        false

      element.bind "dragend", () ->
        @className="dropload"
        false


      element.bind "drop", (evt) ->
        @className = "dropload"
        evt.stopPropagation()
        evt.preventDefault()

        file = evt.originalEvent.dataTransfer.files[0]
        reader = new FileReader()

        reader.onload =  (event) ->
          console.log "Event onload [#{event.target}]"
          scope.load event.target.result
          scope.$apply () ->
            scope.status = "#{file.name} loaded."

        console.log "Reading file [#{file}]"
        reader.readAsDataURL(file)

        false

  )
