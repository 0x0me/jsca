'use strict'

describe 'Directive: dropload', () ->

  # load the directive's module
  beforeEach module 'jscaApp'

  scope = {}

  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()

  it 'should make hidden element visible', inject ($compile) ->
    element = angular.element '<dropload></dropload>'
    element = $compile(element) scope
    expect(element.text()).toBe 'this is the dropload directive'
